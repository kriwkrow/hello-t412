#define LED_A 2
#define LED_B 4
#define BUTTON 3

int blinkDelayOff = 500;
int blinkDelayOn = 50;

void setup() {
  pinMode(LED_A, OUTPUT);
  pinMode(LED_B, OUTPUT);
  pinMode(BUTTON, INPUT_PULLUP);

  Serial.begin(9600);
}

void loop() {
  int onOrOff = digitalRead(BUTTON);
  int delayResult = blinkDelayOff;
  Serial.println(onOrOff);
  
  if (onOrOff) delayResult = blinkDelayOn;
  
  digitalWrite(LED_A, HIGH);
  digitalWrite(LED_B, LOW);
  delay(delayResult);
  digitalWrite(LED_A, LOW);
  digitalWrite(LED_B, HIGH);
  delay(delayResult);
}
